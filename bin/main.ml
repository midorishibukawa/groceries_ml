open Batteries
module D = Dream
module T = Templates

let () = 
    let handle_htmx ~req body =
        let open Dream_html in
        match D.header req "HX-Request" with
        | None   -> respond @@ T.page ~title:"groceries" body
        | Some _ -> respond @@ body in
    D.run ~port:8080 ~interface:"0.0.0.0"
    @@ D.logger 
    @@ D.memory_sessions
    @@ D.router [

        D.get "/static/**" 
            @@ D.static 
            ~loader:(fun _root path _req -> 
                match Static.read path with 
                | None -> Dream.empty `Not_Found 
                | Some asset -> D.respond asset)
            "";

        D.get "/" (fun req -> 
            let open Dream_html in
            let open HTML in
            handle_htmx ~req
            @@ main [] 
                [ h1 [] [txt "groceries"]]);
    ]
