FROM alpine:latest
COPY groceries_ml /groceries_ml
EXPOSE 8080
CMD [ "/groceries_ml" ]
